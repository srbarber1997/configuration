package org.bitbucket.srbarber1997.configuration;

public enum Outcome {
    THROW_EXCEPTION(),
    CREATE_DEFAULT_CONFIGURATION()
}
